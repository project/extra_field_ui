<?php

/**
 * @file
 * Form callbacks for extra_fields configurations.
 */

/**
 * Form constructor for the extra field settings form.
 *
 * @see extra_field_ui_extra_field_edit_form_submit()
 * @ingroup forms
 */
function extra_field_ui_extra_field_edit_form($form, &$form_state, $extra_fields_settings) {
  $bundle = $extra_fields_settings['bundle'];
  $entity_type = $extra_fields_settings['entity_type'];
  $bundles = field_info_bundles($entity_type);
  $label = !empty($extra_fields_settings['label']) ? $extra_fields_settings['label'] : $extra_fields_settings['field_name'];
  $field_type = isset($extra_fields_settings['type']) ? $extra_fields_settings['type'] : '';

  drupal_set_title($label);

  // Sets metadata information so that the settings can be saved.
  $form['entity_type'] = [
    '#type' => 'value',
    '#value' => $entity_type,
  ];
  $form['bundle'] = [
    '#type' => 'value',
    '#value' => $bundle,
  ];
  $form['field_name'] = [
    '#type' => 'value',
    '#value' => $extra_fields_settings['field_name'],
  ];

  // Create a form structure for the extra field settings.
  $form['extra_field'] = [
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('%bundle %field settings', [
      '%bundle' => $bundles[$bundle]['label'],
      '%field' => $extra_fields_settings['label'],
    ]),
    '#description' => t('These settings apply only to the %field field when used in the %bundle bundle.', [
      '%field' => $extra_fields_settings['label'],
      '%bundle' => $bundles[$bundle]['label'],
    ]),
  ];
  $form['extra_field']['weight'] = [
    '#type' => 'value',
    '#value' => !empty($extra_fields_settings['weight']) ? $extra_fields_settings['weight'] : 0,
  ];
  // Build the configurable instance values.
  $form['extra_field']['label'] = [
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $label,
    '#required' => TRUE,
    '#weight' => -20,
  ];
  $form['extra_field']['required'] = [
    '#type' => 'radios',
    '#title' => t('Required field'),
    '#default_value' => isset($extra_fields_settings['required']) ? $extra_fields_settings['required'] : -1,
    '#options' => [-1 => 'N/A', 0 => 'No', 1 => 'Yes'],
    '#weight' => -15,
  ];
  $form['extra_field']['description'] = [
    '#type' => 'textarea',
    '#title' => t('Help text'),
    '#default_value' => isset($extra_fields_settings['description']) ? $extra_fields_settings['description'] : '',
    '#rows' => 5,
    '#description' => t('Instructions to present to the user below this field on the editing form.<br />Allowed HTML tags: @tags',
      [
        '@tags' => _field_filter_xss_display_allowed_tags(),
      ]
    ),
    '#weight' => -10,
  ];
  $form['extra_field']['default_value'] = [
    '#type' => 'textfield',
    '#title' => t('Default value'),
    '#description' => t('The default value for this field, used when creating new content.'),
    '#default_value' => isset($extra_fields_settings['default_value']) ? $extra_fields_settings['default_value'] : NULL,
    '#weight' => -5,
  ];
  $form['extra_field']['type'] = [
    '#type' => 'select',
    '#title' => t('Widget Type'),
    '#default_value' => $field_type,
    '#options' => extra_field_ui_extra_field_types_info(),
    '#empty_option' => '- Default (unchanged) -',
    '#weight' => 0,
  ];
  $options = isset($extra_fields_settings['options']) ? $extra_fields_settings['options'] : [];
  $form['extra_field']['options'] = [
    '#type' => 'textarea',
    '#title' => t('Options List'),
    '#description' => t("This field stores text values from a list of allowed 'value => label' pairs, i.e. 'US States': IL => Illinois, IA => Iowa, IN => Indiana. <br /> Note: This option only has effect for widget types that support it."),
    '#default_value' => list_allowed_values_string($options),
    '#rows' => 10,
    '#element_validate' => array('extra_field_ui_list_allowed_values_setting_validate'),
    '#weight' => 5,
  ];
  $form['extra_field']['active'] = [
    '#type' => 'checkbox',
    '#title' => t('Active'),
    '#description' => t('If this field will be visible on the form.'),
    '#default_value' => isset($extra_fields_settings['active']) ? $extra_fields_settings['active'] : 1,
    '#weight' => 10,
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = ['#type' => 'submit', '#value' => t('Save settings')];
  return $form;
}

/**
 * Form submission handler for field_ui_field_edit_form().
 */
function extra_field_ui_extra_field_edit_form_submit($form, &$form_state) {
  // Retrieves information from the form.
  $extra_field_settings = $form_state['values']['extra_field'];
  // Unsets "empty" values.
  if (empty($extra_field_settings['options'])) {
    unset($extra_field_settings['options']);
  }
  if (empty($extra_field_settings['type'])) {
    unset($extra_field_settings['type']);
  }
  if ('-1' == $extra_field_settings['required']) {
    unset($extra_field_settings['required']);
  }
  if ('' == $extra_field_settings['default_value']) {
    unset($extra_field_settings['default_value']);
  }

  $entity_type = $form_state['values']['entity_type'];
  $bundle = $form_state['values']['bundle'];
  $extra_field = $form_state['values']['field_name'];

  // Appends the 'display' settings since we are only modifying the 'form'.
  $extra_fields = field_bundle_settings($entity_type, $bundle);
  $extra_fields['extra_fields']['form'][$extra_field] = $extra_field_settings;
  // Updates the information back to the variable.
  field_bundle_settings($entity_type, $bundle, $extra_fields);

  drupal_set_message(t('Saved %label configuration.', array('%label' => $extra_field_settings['label'])));

  $admin_path = _field_ui_bundle_admin_path($entity_type, $bundle) . '/fields';

  $form_state['redirect'] = $admin_path;
}

/**
 * Retrieves the list of supported widget types.
 */
function extra_field_ui_extra_field_types_info() {
  return [
    'textfield' => 'Textfield',
    'textarea' => 'Textarea',
    'select' => 'Select List',
    'radios' => 'Radio List',
    'checkbox' => 'Single Checkbox',
  ];
}

/**
 * Validates if the Options List has valid values.
 */
function extra_field_ui_list_allowed_values_setting_validate($element, &$form_state) {
  $values = list_extract_allowed_values($element['#value'], 'list_text', FALSE);

  if (!is_array($values)) {
    form_error($element, t('Options List> Invalid input.'));
    return;
  }

  // Check that keys are valid for the field type.
  foreach ($values as $key => $value) {
    if (drupal_strlen($key) > 255) {
      form_error($element, t('Options List: Each key must be a string at most 255 characters long.'));
      break;
    }
  }

  form_set_value($element, $values, $form_state);
}
