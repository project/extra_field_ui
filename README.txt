INTRODUCTION
------------

The Extra Field UI module provides an easier way for developers to configure and
display extra fields on any Entity Form (including node and user).
*If you are not a developer then you probably shouldn't enable this module.*


MAINTAINERS
-----------

Current maintainers:
  * Felipe Caminada - https://www.drupal.org/u/caminadaf

This project has been sponsored by:
  * CI&T - CI&T has more experience than anyone else in leading Drupal
    implementations for multi-brand enterprises. When you need to provide better
    more timely, and relevant web experiences for your customers, CI&T takes
    you where you need to be. (http://drupal.org/node/1530378)


REQUIREMENTS
------------

  * This module depends on the "field_ui" module.


INSTALLATION
------------

No special installation needed.


CONFIGURATION
------------
  * To use this module you need to have at least one extra_field on your entity.
  * Alternatively, you can enable the entity_property_field module and have
    all your properties to be automatically converted into extra_fields.
  * On every entity "Manage Fields" UI, all the extra fields of that entity
    will have an "edit" link on its row.
  * On the extra_field configuration page you will be able to add settings for:
    - Label
    - Required
    - Help text
    - Default value
    - Widget Type
    - Options List
    - Active
  * Each configured settings will then be reflected on that entity's add/edit
    page.
    Please note that there are some configuration sets that are incompatible.
    Example: "textfield" type with "options list" set.
  * This is specially useful if you use this value on the backend for any
    custom processing or if you have the entity_property_field module enabled.
